

import 'package:b2_sdk_flutter/src/common/defines.dart';

import 'common/models.dart';
import 'common/utils.dart';
import 'account.dart';
import 'upload.dart';

class B2 {
  static final B2 _instance = B2._();
  B2._();
  factory B2() {
    return _instance;
  }

  String accountId; // <YOUR B2 ACCOUNT ID>
  String applicationKey; // <YOUR B2 APPLICATION KEY>

  B2Account _b2Account;
  Map<String, B2UploadUrl> _uploadUrls = Map<String, B2UploadUrl>();

  /// Accounts
  ///
  Future<B2Account> authorizeAccount() async {
    return await B2AccountAgent.authorizeAccount(accountId, applicationKey);
  }

  /// Upload
  ///
  Future<B2UploadUrl> getUploadUrl(B2Account account, String bucketId) async {
    return await B2UploadAgent.getUploadUrl(account, bucketId);
  }

  Future<B2File> uploadFileWithPath(B2UploadUrl uploadUrl, String filePath, String filename, {String sha1Checksum, String contentType, int contentLength}) async {
    return await B2UploadAgent.uploadFileWithPath(uploadUrl, filePath, filename, sha1Checksum: sha1Checksum, contentType: contentType, contentLength: contentLength);
  }

  Future<B2File> uploadFileWithData(B2UploadUrl uploadUrl, List<int> fileData, String filename, {String sha1Checksum, String contentType, int contentLength}) async {
    return await B2UploadAgent.uploadFileWithData(uploadUrl, fileData, filename, sha1Checksum: sha1Checksum, contentType: contentType, contentLength: contentLength);
  }

  Future<B2File> uploadFileData(List<int> fileData, String filename, String bucketId, {String sha1Checksum, String contentType, int contentLength}) async {
    B2Account b2Account = _b2Account;
    if (b2Account == null) {
      b2Account = await authorizeAccount();
    }

    B2UploadUrl uploadUrl = _uploadUrls[bucketId];
    if (uploadUrl == null) {
      uploadUrl = await getUploadUrl(b2Account, bucketId);
    }

    _b2Account = b2Account;
    _uploadUrls[bucketId] = uploadUrl;

    try {
      if (sha1Checksum == null || contentType == null) {
        Map<String, String> headers = await B2Utils.httpHeadersForFileData(fileData);

        sha1Checksum = sha1Checksum ?? headers[B2HTTPHeaders.contentSHA1];
        contentType = contentType ?? headers[B2HTTPHeaders.contentType];
      }

      return await uploadFileWithData(uploadUrl, fileData, filename, sha1Checksum: sha1Checksum, contentType: contentType, contentLength: contentLength);
    }
    catch (e) {
      reset();
      throw e;
    }
  }

  void reset() {
    _b2Account = null;
    _uploadUrls.clear();
  }
}