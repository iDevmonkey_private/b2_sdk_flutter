import 'package:http/http.dart' as h;

import 'common/base.dart';
import 'common/models.dart';
import 'common/defines.dart';
import 'common/utils.dart';

class B2AccountAgent extends IB2Agent {
  static Future<B2Account> authorizeAccount(String accountId, String applicationKey) async {
    Uri uri = Uri.parse(B2Urls.authorizeAccountUrl());
    Map<String, String> headers = {B2HTTPHeaders.authorization: B2Utils.authorizeAccountPayload(accountId, applicationKey)};

    var client = h.Client();
    var response = await client.get(uri, headers: headers);
    var result = await IB2Agent.onResponse(response);
    return B2Account.fromString(result);
  }
}