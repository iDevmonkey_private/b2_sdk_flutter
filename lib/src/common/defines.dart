
class B2Urls {
  static String authorizeAccountUrl() => "https://api.backblazeb2.com/b2api/v1/b2_authorize_account";
  static String getUploadUrl(String baseUrl) => "$baseUrl/b2api/v1/b2_get_upload_url";
  static String downloadFileById(String baseUrl) => "$baseUrl/b2api/v1/b2_download_file_by_id";
}

class B2HTTPHeaders {
  static const String contentSHA1 = "X-Bz-Content-Sha1";
  static const String contentType = "Content-Type";
  static const String contentLength = "Content-Length";
  static const String authorization = "Authorization";
  static const String fileName = "X-Bz-File-Name";

  static const String valueAutoOfContentType = "b2/x-auto";
  static const String valueNotVerifyOfContentSHA1 = "do_not_verify";
}

class B2Defines {
  static const int uploadRetryDefault = 1;
}